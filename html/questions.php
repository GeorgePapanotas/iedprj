<?php
include_once 'dbh.inc.php';
include_once 'user.inc.php';
include_once 'quest.inc.php';
session_start();
if(isset($_SESSION['firstname'])){
    $firstname = $_SESSION['firstname'];
}
if(isset($_SESSION['lastname'])){
    $lastname = $_SESSION['lastname'];
}
if(isset($_SESSION['rank'])){
    $rank = $_SESSION['rank'];
}
if(isset($_SESSION['userID'])){
    $userid = $_SESSION['userID'];
}
if(isset($_SESSION['Period'])){
    $period = $_SESSION['Period'];
}


?>

<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
    <meta charset="utf-8">
    <title>Questionare</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.0/css/bootstrap.min.css" integrity="sha384-PDle/QlgIONtM1aqA2Qemk5gPOE7wFq8+Em+G/hmo5Iq0CCmYZLv3fVRDJ4MMwEA" crossorigin="anonymous">
    <link rel="stylesheet" href="../css/styles.css">
    <link rel="stylesheet" href="../css/questions.css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Comfortaa|Open+Sans+Condensed:300|Roboto+Slab" rel="stylesheet">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.0/js/bootstrap.min.js" integrity="sha384-7aThvCh9TypR7fIc2HV4O/nFMVCBwyIUKL8XCtKE+8xgCgl/PQGuFsvShjr74PBp" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
</head>
<body>
<!-- header -->
<div class="heading">
    <div class="row">
        <div class="col-lg-6 col-md-12 image">
            <img src="https://ied.eu/wp-content/uploads/2018/11/full-logo-text-homepage.png" alt="dog-profile" class="header-img">
        </div>
        <div class="col-lg-6 col-md-12 title">
            <h3>Πρόγραμμα Aξιολόγησης Προσωπικού</h3>
        </div>
    </div>
    <hr/>
</div>
<!-- end of header -->
<!--questions -->
<h2>Please Fill Up The Following Inputs</h2>
<?php
$LoggedUser = new User($userid,$firstname,$lastname,$rank);
$test1 = $LoggedUser->getPeople();
$test2 = array_unique($test1);
$test = array();
foreach($test2 as $t){
  array_push($test,$t);
}

$answers = array();
?>

<div class="questionares">
    <div class="accordion" id="accordionExample">
      <form action="test1.php" method="post">

        <?php
        $object = new questions();
        for($i = 0;$i<sizeof($test);$i++){
            $object1 = new User($test[$i]->getUserid(),$test[$i]->getFirstname(),$test[$i]->getLastname(),$test[$i]->getPosition());
            $questions = $object1-> getSingleQuestions($object1);
            if($test[$i]->getFirstname() != $firstname && $test[$i]->getLastname() != $lastname){
                $qid = 1;
                $object->constructTemplateHeader($test[$i]->getFirstname(),$test[$i]->getLastname(),$test[$i]->getPositionText(),$qid);
                foreach($questions as $q){
                    $object->setQID($q[0]);
                    $object->setQText($q[1]);
                    $object->setQuestID($q[2]);
                    $object->constructTemplateBody($test[$i]->getLastname());
                    array_push($answers,$test[$i]->getLastname().$object->getQID());
                }
                echo '</div>
            </div>
            </div>
            ';
            }

        }

		if($LoggedUser->isSupervisor() && $LoggedUser->getLastname() != 'Σιακαβέλης'){
			$object = new questions();
			$it = $LoggedUser->getIt();
			$object->constructTemplateHeader($LoggedUser->getFirstname(),$LoggedUser->getLastname(),$LoggedUser->getPositionText(),6);
                foreach($it as $q){
                    $object->setQID($q[0]);
                    $object->setQText($q[1]);
                    $object->setQuestID($q[2]);
                    $object->constructTemplateBody("IT");
                    array_push($answers,"IT".$object->getQID());
                }
                echo '</div>
            </div>
            </div>
            ';
		}

        $qid = 2;
        $self = $LoggedUser->getSelfEval();
		$object = new questions();
        $object->constructTemplateHeader($LoggedUser->getFirstname(),$LoggedUser->getLastname(),$LoggedUser->getPositionText(),$qid);
        foreach($self as $q){
          $object->setQID($q[0]);
          $object->setQText($q[1]);
          $object->setQuestID($q[2]);
            $object->constructTemplateBody($LoggedUser->getLastname());
            array_push($answers,$LoggedUser->getLastname().$object->getQID());
        }
        echo '</div>
          </div>
          </div>';


        if($LoggedUser->isSupervisor()){
			      $object = new questions();
            if($LoggedUser->checkTeamwordAnswerd($period)){
            $fourth = $LoggedUser->getTeamwork();
            $DeptColligues = $LoggedUser->getUserColligues();

            for($i = 0;$i<sizeof($DeptColligues);$i++){
                $qid = 4;
                $object->setQID($fourth[0][0]);
                $object->setQText($fourth[0][1]);
                $object->setQuestID($fourth[0][2]);
                $object->constructTemplateHeader($DeptColligues[$i]->getFirstname(),$DeptColligues[$i]->getLastname(),$DeptColligues[$i]->getPositionText(),$qid);

                $object->constructTemplateBody($DeptColligues[$i]->getLastname());
                echo $DeptColligues[$i]->getLastname().$object->getQID();
                array_push($answers,$DeptColligues[$i]->getLastname().$object->getQID());
                echo '</div>
              </div>
              </div>';
            }
          }
			if($LoggedUser->checkDeptAnswered($period)){

			      $object = new questions();
            $goals = $LoggedUser->getDepartmentGoals();
			      $dept2 = $LoggedUser->getUserDepartment();
            $qid = 5;
            $object->constructTemplateHeader("goal","goal",1,$qid);
            foreach($goals as $q){
              $object->setQID($q[0]);
              $object->setQText($q[1]);
              $object->setQuestID($q[2]);
                $object->constructTemplateBody('dept');
                array_push($answers,'dept'.$object->getQID());
            }
            echo '</div>
              </div>
			</div>';
			}
        }

        $_SESSION['answers'] = $answers;
        $_SESSION['User'] = $LoggedUser;


        echo '<button type="submit" class="btn btn-lg btn-outline-info">Submit  <i class="fas fa-chevron-right"></i></button>
              </form>
          </div>
          </div>';


        ?>


</body>
</html>
