<?php
include_once 'dbh.inc.php';
include_once 'user.inc.php';
include_once 'quest.inc.php';
session_start();
    if(isset($_SESSION['firstname'])){
      $firstname = $_SESSION['firstname'];
    }
    if(isset($_SESSION['lastname'])){
      $lastname = $_SESSION['lastname'];
    }
    if(isset($_SESSION['rank'])){
      $rank = $_SESSION['rank'];
    }
    if(isset($_SESSION['userID'])){
      $userid = $_SESSION['userID'];
    }
?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>tst</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.0/css/bootstrap.min.css" integrity="sha384-PDle/QlgIONtM1aqA2Qemk5gPOE7wFq8+Em+G/hmo5Iq0CCmYZLv3fVRDJ4MMwEA" crossorigin="anonymous">
    <link rel="stylesheet" href="../css/styles.css">
    <link rel="stylesheet" href="../css/questions.css">
    <link href="https://fonts.googleapis.com/css?family=Comfortaa|Open+Sans+Condensed:300|Roboto+Slab" rel="stylesheet">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.0/js/bootstrap.min.js" integrity="sha384-7aThvCh9TypR7fIc2HV4O/nFMVCBwyIUKL8XCtKE+8xgCgl/PQGuFsvShjr74PBp" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
  </head>
  <body>
    <?php
    $object = new User($userid,$firstname,$lastname,$rank);
    $test = $object->getPeople();
    //$questions = $object-> getSingleQuestions($object);

       ?>

       <div class="questionares">
         <div class="accordion" id="accordionExample">
          <?php
          $object = new questions();
          //$object->constructTemplateHeader('George','Papanotas','Software Developer',1);
          for($i = 0;$i<sizeof($test);$i++){
            $object1 = new User($test[$i]->getUserid(),$test[$i]->getFirstname(),$test[$i]->getLastname(),$test[$i]->getPosition());
            $questions = $object1-> getSingleQuestions($object1);
            $self = $object1->getSelfEval();
            if($test[$i]->getFirstname() == $firstname && $test[$i]->getLastname() == $lastname){
              $qid = 2;
              $object->constructTemplateHeader($test[$i]->getFirstname(),$test[$i]->getLastname(),$test[$i]->getPositionText(),$qid);
              foreach($self as $q){
                $object->constructTemplateBody($q[0]);
              }
            }else{
              $qid = 1;
              $object->constructTemplateHeader($test[$i]->getFirstname(),$test[$i]->getLastname(),$test[$i]->getPositionText(),$qid);
              foreach($questions as $q){
                $object->constructTemplateBody($q[0]);
              }
            }

            echo '</div>
            </div>
            </div>';
          }
         ?>

       </div>
     </div>
       </div>

  </body>
</html>
