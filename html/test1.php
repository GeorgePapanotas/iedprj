<?php
include_once 'dbh.inc.php';
include_once 'user.inc.php';
include_once 'quest.inc.php';

session_start();
$r =  $_SESSION['User'];
$r->getLastname();
$per = $_SESSION['Period'];
if (isset($_SESSION['answers'])){
$answers = $_SESSION['answers'];

foreach($answers as $a){
  $target = substr($a,0,strlen($a)-strlen(filter_var($a, FILTER_SANITIZE_NUMBER_INT)));
  $questid = filter_var($a, FILTER_SANITIZE_NUMBER_INT);
  $result = $r->getMult($questid);
  $answer = $_POST[$a];

  $r->insertData($r->getUserid(),$questid,$target,$per,$answer,($answer*$result));
}
echo $per;
if($r->checkAllAnswerd($per)){
  $ppl = $r->getParticipants($per);
  foreach($ppl as $p){
    if($p[0]->isSupervisor()){
      $coll1 = $p[0]->getSupervisorAnswers($per);
      $coll2 = $p[0]->getJuniorAnswers($per);
      $coll = (($coll1 + $coll2)/2);
    }else{
        $coll = $p[0]->getColligueEval($per);
    }
    $self = $p[0]->getSelfEvaluationResults($per);
    $team = $p[0]->getTeamworkResults($per);
    $dept = (($p[0]->getDeptResults($per)[sizeof($p[0]->getDeptResults($per))-1])*100);
    $person = (($p[0]->getNumber($p[0]->getDeptResults($per)))*100);

    //$Tot = ($coll*0.19+ $self*0.08+$team*0.24+$dept*0.25+$person*0.24)/10;


    // TODO: Add those to last collumn
    // TODO: 0.12 0.07 0.08 0.24 0.25 0.24 Supervisors & ceo
    // TODO: 0.19 0.08 0.24 0.25 0.24      Juniors


    if($p[0]->isSupervisor()){
      $total = $coll2*0.12+$coll1*0.07 + $self*0.08 + $team*0.24 + $dept * 0.25 + $person*0.24;
    }else{
      $total = $coll*0.19 + $self*0.08 + $team*0.24 + $dept * 0.25 + $person*0.24;
    }

    $kappa = $p[0]->getResults($per);

    if(sizeof($p[0]->getResults($per)) == 0){
      if(is_null($team)){
        $team = 0;
      }
      $p[0]->InsertResults($coll,$self,$team,$dept,$person,$total,$per,$p[0]->getUserId());
    }

  }
}
  header('Location: main.php');

}

?>
