-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 12, 2019 at 01:19 PM
-- Server version: 10.1.36-MariaDB
-- PHP Version: 7.2.10
CREATE DATABASE IF NOT EXISTS systimaaksiologisis;
USE systimaaksiologisis;

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `systimaaksiologisis`
--

-- --------------------------------------------------------

--
-- Table structure for table `answers`
--

CREATE TABLE `answers` (
  `userID` int(11) NOT NULL,
  `QuestID` int(11) NOT NULL,
  `target` int(11) DEFAULT NULL,
  `period` text NOT NULL,
  `Ans1` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `credentials`
--

CREATE TABLE `credentials` (
  `username` varchar(36) NOT NULL,
  `password1` varchar(36) NOT NULL,
  `userID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `credentials`
--

INSERT INTO `credentials` (`username`, `password1`, `userID`) VALUES
('Tester', 'Tester', 1),
('Tester2', 'Tester', 2),
('Tester3', 'Tester', 3),
('Tester4', 'Tester', 5);

-- --------------------------------------------------------

--
-- Table structure for table `ieddepartment`
--

CREATE TABLE `ieddepartment` (
  `DeptID` int(11) NOT NULL,
  `Title` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ieddepartment`
--

INSERT INTO `ieddepartment` (`DeptID`, `Title`) VALUES
(1, 'Implementation'),
(2, 'Submission'),
(3, 'IT'),
(4, 'Marketing'),
(5, 'Managment');

-- --------------------------------------------------------

--
-- Table structure for table `ieduser`
--

CREATE TABLE `ieduser` (
  `userID` int(11) NOT NULL,
  `firstname` varchar(20) NOT NULL,
  `lastname` varchar(20) NOT NULL,
  `rank1` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ieduser`
--

INSERT INTO `ieduser` (`userID`, `firstname`, `lastname`, `rank1`) VALUES
(1, 'George', 'Papanotas', 3),
(2, 'Anne', 'Warren', 3),
(3, 'Adrian', 'Woodward', 4),
(4, 'Isla', 'Mccall', 8),
(5, 'Emma', 'Larsen', 9),
(6, 'Julia', 'Cortez', 10);

-- --------------------------------------------------------

--
-- Table structure for table `positions`
--

CREATE TABLE `positions` (
  `positionID` int(11) NOT NULL,
  `Title` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `positions`
--

INSERT INTO `positions` (`positionID`, `Title`) VALUES
(1, 'Project Manager'),
(2, 'Supervisor - Lead Project Manager'),
(3, 'Designer'),
(4, 'Supervisor - Developer'),
(5, 'Secretary'),
(6, 'Supervisor - CEO'),
(7, 'Content Manager'),
(8, 'SEO Expert Copyrighter'),
(9, 'Social Media Manager'),
(10, 'Supervisor - Marketing and Innovation Manager'),
(11, 'Fundrising Network Assistant'),
(12, 'Proposal Writer'),
(13, 'Supervisor - Fundrising Network Manager'),
(14, 'Supervisor - Sub Planner');

-- --------------------------------------------------------

--
-- Table structure for table `questionares`
--

CREATE TABLE `questionares` (
  `QID` int(11) NOT NULL,
  `QType` varchar(60) NOT NULL,
  `QSubtype` varchar(60) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `questionares`
--

INSERT INTO `questionares` (`QID`, `QType`, `QSubtype`) VALUES
(1, '1', 'Project Manager'),
(2, '1', 'Supervisor - Lead Project Manager'),
(3, '1', 'All'),
(4, '1', 'Designer'),
(5, '1', 'Supervisor - Developer'),
(6, '1', 'Secretary'),
(7, '1', 'Supervisor - CEO'),
(8, '1', 'Content Manager'),
(9, '1', 'SEO Expert Copyrighter'),
(10, '1', 'Social Media Manager'),
(11, '1', 'Supervisor - Marketing and Innovation Manager'),
(12, '1', 'Fundrising Network Assistant'),
(13, '1', 'Proposal Writer'),
(14, '1', 'Supervisor - Fundrising Network Manager'),
(15, '1', 'Supervisor - Sub Planner'),
(16, '2', 'Self Evaluation'),
(17, '4', 'Teamwork'),
(18, '5', 'Implementation Goals'),
(19, '5', 'IT Goals'),
(20, '5', 'Managment Goals'),
(21, '5', 'Marketing Goals'),
(22, '5', 'Submissions Goals');

-- --------------------------------------------------------

--
-- Table structure for table `questions`
--

CREATE TABLE `questions` (
  `QuestID` int(11) NOT NULL,
  `QText` text NOT NULL,
  `QID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `questions`
--

INSERT INTO `questions` (`QuestID`, `QText`, `QID`) VALUES
(1, 'Quality of web development', 3),
(2, 'Speed of web development', 3),
(3, 'Graphic development quality / appearance', 3),
(4, 'Graphic development speed', 3),
(5, 'User support response time', 3),
(6, 'User support know-how', 3),
(7, 'User support to third parties or others', 3),
(8, 'Innovation and new ideas', 3),
(9, 'New systems developed', 3),
(10, 'Attempt for individual improvement and new knowledge', 4),
(11, 'Keeps working hours', 4),
(12, 'Knowledge of the function of their department', 4),
(13, 'Knowledge of iED', 4),
(14, 'Flexibility and adaptability to change', 4),
(15, 'Taking responsibility', 4),
(16, 'Problem Solving Capability', 4),
(17, 'Ability to work under pressure', 4),
(18, 'Collaboration/Communication with colleagues and/or external partners', 4),
(19, 'Interested, excited and willing to work', 4),
(20, 'Accepts advice and instructions', 4),
(21, 'General Organization', 4),
(22, 'Ability to perform work with precision', 4),
(23, 'Ability to perform work consistently', 4),
(24, 'Knowledge of trends / hot topics / designing', 4),
(25, 'Speed of work', 4),
(26, 'Graphic design quality', 4),
(27, 'User support', 4),
(28, 'Attempt for individual improvement and new knowledge', 5),
(29, 'Keeps working hours', 5),
(30, 'Knowledge of the function of their department', 5),
(31, 'Knowledge of iED', 5),
(32, 'Flexibility and adaptability to change', 5),
(33, 'Taking responsibility', 5),
(34, 'Problem Solving Capability', 5),
(35, 'Ability to work under pressure', 5),
(36, 'Collaboration/Communication with colleagues and/or external partners', 5),
(37, 'Interested, excited and willing to work', 5),
(38, 'Accepts advice and instructions', 5),
(39, 'General organization', 5),
(40, 'Ability to perform work with precision', 5),
(41, 'Ability to perform work consistently', 5),
(42, 'Knowledge of trends / hot topics / programming', 5),
(43, 'Speed of work', 5),
(44, 'Code writing quality', 5),
(45, 'User support', 5),
(46, 'Quality of work', 1),
(47, 'Speed ​​of work', 1),
(48, 'Attempt for individual improvement', 1),
(49, 'Keeps the deadlines', 1),
(50, 'Keeps work hours', 1),
(51, 'Knowledge of the department', 1),
(52, 'Knowledge of iED', 1),
(53, 'Knowledge of iED deliverables', 1),
(54, 'Flexibility and adaptability to change', 1),
(55, 'Taking responsibility', 1),
(56, 'Personal effort', 1),
(57, 'Problem Solving Capability', 1),
(58, 'Ability to work under pressure', 1),
(59, 'Communication with colleagues', 1),
(60, 'Interested, excited and willing to work', 1),
(61, 'Accept advice and instructions', 1),
(62, 'Willingness for education and new knowledge', 1),
(63, 'Volunteering and Sensitivity', 1),
(64, 'Collaboration with colleagues and external partners', 1),
(65, 'Offer help and willingness to colleagues', 1),
(66, 'Interpersonal relationships with colleagues', 1),
(67, 'Courtesy / friendliness', 1),
(68, 'Systematically organizing', 1),
(69, 'Ability to work with precision', 1),
(70, 'Ability to work consistently', 1),
(71, 'IT knowledge', 1),
(72, 'organization / management', 1),
(73, 'Knowledge of trends / hot topics', 1),
(74, 'Ability to manage time / work', 1),
(75, 'Quality of work', 2),
(76, 'Speed ​​of work execution', 2),
(77, 'Attempt for individual improvement', 2),
(78, 'Consequence of work', 2),
(79, 'Keeping hours', 2),
(80, 'Attempt to improve his / her work', 2),
(81, 'Knowledge of the function of the department', 2),
(82, 'Knowledge of iED', 2),
(83, 'Knowledge of iED deliverables', 2),
(84, 'Flexibility and adaptability to change', 2),
(85, 'Taking responsibility', 2),
(86, 'Personal effort', 2),
(87, 'Problem Solving Capability', 2),
(88, 'Ability to work under pressure', 2),
(89, 'Contact with colleagues', 2),
(90, 'Interesting, excited and willing to work', 2),
(91, 'Accept advice and instructions', 2),
(92, 'Willingness for education and new knowledge', 2),
(93, 'Volunteering and Sensitivity', 2),
(94, 'Collaboration with colleagues and external partners', 2),
(95, 'Offer help, willingness and service to colleagues', 2),
(96, 'Interpersonal relationships with colleagues', 2),
(97, 'Courtesy / friendliness', 2),
(98, 'Systematically organizing and operating the iED', 2),
(99, 'Ability to perform work with precision', 2),
(100, 'IT knowledge', 2),
(101, 'Knowledge of organization / management', 2),
(102, 'Knowledge of trends / hot topics', 2),
(103, 'Ability to manage time / work', 2),
(104, 'Θεωρείς ότι ο τρόπος διαχείρισης και οργάνωσης του τμήματος συμβάλλει στην επίτευξη των στόχων?', 2),
(105, 'Θεωρείς ότι η κατανομή εργασιών και αρμοδιοτήτων είναι δίκαιη και καλύπτει τις ανάγκες του τμήματος?', 2),
(106, 'Τα όποια προβλήματα / θέματα προκύπτουν εντός του τμήματος λύνονται άμεσα και εύκολα?', 2),
(107, 'Θεωρείς ότι υπάρχει ίση αντιμετώπιση όλων των ατόμων από τον υπεύθυνο του τμήματος?', 2),
(108, 'Quality of work', 6),
(109, 'Speed ​​of work execution', 6),
(110, 'Consequence of work', 6),
(111, 'Keeping hours', 6),
(112, 'Attempt to improve his / her work', 6),
(113, 'Knowledge of the function of the department', 6),
(114, 'Knowledge of iED', 6),
(115, 'Flexibility and adaptability to change', 6),
(116, 'Taking responsibility', 6),
(117, 'Problem Solving Capability', 6),
(118, 'Ability to work under pressure', 6),
(119, 'Contact with colleagues', 6),
(120, 'Interesting, excited and willing to work', 6),
(121, 'Volunteering and Sensitivity', 6),
(122, 'Collaboration with colleagues and external partners', 6),
(123, 'Offer help, willingness and service to colleagues', 6),
(124, 'Courtesy / friendliness', 6),
(125, 'Systematically organizing and operating the iED', 6),
(126, 'Βαθμολογήστε την καθαριότητα του χώρου', 6),
(127, 'Βαθμολογήστε την  κράτηση εισητηρίων ως προς το κόστος τους', 6),
(128, 'Βαθμολογήστε την αποτελεσματικότητα οργάνωσης εκδηλώσεων/συναντήσεων στο Ied', 6),
(129, 'Βαθμολογήστε την οικονομική διαχείριση του Ταμείου', 6),
(130, 'Βαθμολογήστε την εξυπηρέρηση και πληροφόρηση που παρέχεται σε τρίτους τηλεφωνικά και διαζώσης', 6),
(131, 'Δημιουργήθηκαν προβλήματα στην επικοινωνία; ', 6),
(132, 'Έγινε έλεγχος οικονομικών τον προηγούμενο μήνα;', 6),
(133, 'Attempt for individual improvement', 7),
(134, 'Consequence of work', 7),
(135, 'Attempt to improve his / her work', 7),
(136, 'Knowledge of iED', 7),
(137, 'Flexibility and adaptability to change', 7),
(138, 'Taking responsibility', 7),
(139, 'Personal effort', 7),
(140, 'Problem Solving Capability', 7),
(141, 'Ability to work under pressure', 7),
(142, 'Contact with colleagues', 7),
(143, 'Interesting, excited and willing to work', 7),
(144, 'Accept advice and instructions', 7),
(145, 'Willingness for education and new knowledge', 7),
(146, 'Collaboration with colleagues and external partners', 7),
(147, 'Interpersonal relationships with colleagues', 7),
(148, 'Courtesy / friendliness', 7),
(149, 'Systematically organizing and operating the iED', 7),
(150, 'Knowledge of organization / management', 7),
(151, 'Knowledge of trends / hot topics', 7),
(152, 'Πως πιστεύετε ότι ήταν η οικονομική διαχείριση το προηγούμενο διάστημα;', 7),
(153, 'Βαθμολογήστε το συντονισμό του iED από το CEO', 7),
(154, 'Βαθμολογήστε τον τρόπο διοίκησης του προσωπικού από τον CEO', 7),
(155, 'Βαθμολογήστε την αποτελεσματικότητα της στρατηγικής ανάπτυξης του iED ', 7),
(156, 'Quality of work', 8),
(157, 'Speed ​​of work execution', 8),
(158, 'Attempt for individual improvement', 8),
(159, 'Consequence of work', 8),
(160, 'Keeping hours', 8),
(161, 'Attempt to improve his / her work', 8),
(162, 'Knowledge of the function of the department', 8),
(163, 'Knowledge of iED', 8),
(164, 'Knowledge of iED deliverables', 8),
(165, 'Flexibility and adaptability to change', 8),
(166, 'Taking responsibility', 8),
(167, 'Personal effort', 8),
(168, 'Problem Solving Capability', 8),
(169, 'Ability to work under pressure', 8),
(170, 'Contact with colleagues', 8),
(171, 'Interesting, excited and willing to work', 8),
(172, 'Accept advice and instructions', 8),
(173, 'Willingness for education and new knowledge', 8),
(174, 'Volunteering and Sensitivity', 8),
(175, 'Collaboration with colleagues and external partners', 8),
(176, 'Offer help, willingness and service to colleagues', 8),
(177, 'Interpersonal relationships with colleagues', 8),
(178, 'Courtesy / friendliness', 8),
(179, 'Systematically organizing and operating the iED', 8),
(180, 'Ability to perform work at a speed', 8),
(181, 'Ability to perform work with precision', 8),
(182, 'Ability to perform work consistently', 8),
(183, 'IT knowledge', 8),
(184, 'Knowledge of organization / management', 8),
(185, 'Knowledge of trends / hot topics', 8),
(186, 'Σε ποιο βαθμό πιστεύετε ότι ο Content Manager ανταποκρίνεται στους στόχους της στρατηγικής περιεχομένου', 8),
(187, 'Πόσο αποτελεσματική είναι η επικοινωνία σας με τον Content Manager όσον αφορά τον προγραμματισμό άρθρων;', 8),
(188, 'Πως αξιολογείτε τη βοήθεια που λαμβάνετε σε σχέση με τη βελτίωση της σχεδίασης της σελίδας του IED;', 8),
(189, 'Πόσο αποτελεσματική είναι η επικοινωνία σας με τον Content Manager;', 8),
(190, 'Πόσο αποτελεσματική είναι η επικοινωνία και η συνεργασία σας με τον Content Manager;', 8),
(191, 'Quality of work', 9),
(192, 'Attempt for individual improvement', 9),
(193, 'Consequence of work', 9),
(194, 'Keeping hours', 9),
(195, 'Attempt to improve his / her work', 9),
(196, 'Knowledge of the function of the department', 9),
(197, 'Knowledge of iED', 9),
(198, 'Knowledge of iED deliverables', 9),
(199, 'Flexibility and adaptability to change', 9),
(200, 'Taking responsibility', 9),
(201, 'Personal effort', 9),
(202, 'Problem Solving Capability', 9),
(203, 'Ability to work under pressure', 9),
(204, 'Contact with colleagues', 9),
(205, 'Interesting, excited and willing to work', 9),
(206, 'Accept advice and instructions', 9),
(207, 'Willingness for education and new knowledge', 9),
(208, 'Collaboration with colleagues and external partners', 9),
(209, 'Offer help, willingness and service to colleagues', 9),
(210, 'Interpersonal relationships with colleagues', 9),
(211, 'Courtesy / friendliness', 9),
(212, 'Systematically organizing and operating the iED', 9),
(213, 'Ability to perform work with precision', 9),
(214, 'Ability to perform work consistently', 9),
(215, 'IT knowledge', 9),
(216, 'Knowledge of organization / management', 9),
(217, 'Knowledge of trends / hot topics', 9),
(218, 'Σε ποιο βαθμό πιστεύετε ότι ο SEO Expert/Copywriter ανταποκρίνεται στους στόχους του online/digital marketing', 9),
(219, 'Πόσο αποτελεσματική είναι η επικοινωνία σας με τον SEO Expert/Copywriter;', 9),
(220, 'Πως αξιολογείτε τη βοήθεια που λαμβάνετε σε σχέση με τη βελτίωση της σχεδίασης της σελίδας του IED;', 9),
(221, 'Πόσο αποτελεσματική είναι η επικοινωνία σας με τον SEO Expert/Copywriter;', 9),
(222, 'Πόσο αποτελεσματική είναι η επικοινωνία και η συνεργασία σας με τον Content Manager;', 9),
(223, 'Πως αξιολογείτε τη γνώση του SEO Expert/Copywriter σχετικά με τις τελευταίες εξελίσεις σε SEO τεχνικές', 9),
(224, 'Πως αξιολογείτε τo copy writing του SEO Expert/Copywriter;', 9),
(225, 'Quality of work', 10),
(226, 'Speed ​​of work execution', 10),
(227, 'Attempt for individual improvement', 10),
(228, 'Consequence of work', 10),
(229, 'Keeping hours', 10),
(230, 'Attempt to improve his / her work', 10),
(231, 'Knowledge of the function of the department', 10),
(232, 'Knowledge of iED', 10),
(233, 'Knowledge of iED deliverables', 10),
(234, 'Flexibility and adaptability to change', 10),
(235, 'Taking responsibility', 10),
(236, 'Personal effort', 10),
(237, 'Problem Solving Capability', 10),
(238, 'Ability to work under pressure', 10),
(239, 'Contact with colleagues', 10),
(240, 'Interesting, excited and willing to work', 10),
(241, 'Accept advice and instructions', 10),
(242, 'Willingness for education and new knowledge', 10),
(243, 'Volunteering and Sensitivity', 10),
(244, 'Collaboration with colleagues and external partners', 10),
(245, 'Offer help, willingness and service to colleagues', 10),
(246, 'Interpersonal relationships with colleagues', 10),
(247, 'Courtesy / friendliness', 10),
(248, 'Systematically organizing and operating the iED', 10),
(249, 'Ability to perform work at a speed', 10),
(250, 'Ability to perform work with precision', 10),
(251, 'Ability to perform work consistently', 10),
(252, 'IT knowledge', 10),
(253, 'Knowledge of organization / management', 10),
(254, 'Knowledge of trends / hot topics', 10),
(255, 'Σε ποιο βαθμό πιστεύετε ότι ο Sοcial Media Manager ανταποκρίνεται στους στόχους του;', 10),
(256, 'Πόσο αποτελεσματική είναι η επικοινωνία σας με τον Sοcial Media Manager;', 10),
(257, 'Πόσο αποτελεσματική είναι η επικοινωνία και η συνεργασία σας με τον Sοcial Media Manager;', 10),
(258, 'Πως αξιολογείτε τη γνώση του Sοcial Media Manager σχετικά με τις τελευταίες εξελίσεις στα social media;', 10),
(259, 'Quality of work', 11),
(260, 'Speed ​​of work execution', 11),
(261, 'Attempt for individual improvement', 11),
(262, 'Consequence of work', 11),
(263, 'Keeping hours', 11),
(264, 'Attempt to improve his / her work', 11),
(265, 'Knowledge of the function of the department', 11),
(266, 'Knowledge of iED', 11),
(267, 'Knowledge of iED deliverables', 11),
(268, 'Flexibility and adaptability to change', 11),
(269, 'Taking responsibility', 11),
(270, 'Personal effort', 11),
(271, 'Problem Solving Capability', 11),
(272, 'Ability to work under pressure', 11),
(273, 'Contact with colleagues', 11),
(274, 'Interesting, excited and willing to work', 11),
(275, 'Accept advice and instructions', 11),
(276, 'Willingness for education and new knowledge', 11),
(277, 'Volunteering and Sensitivity', 11),
(278, 'Collaboration with colleagues and external partners', 11),
(279, 'Offer help, willingness and service to colleagues', 11),
(280, 'Interpersonal relationships with colleagues', 11),
(281, 'Courtesy / friendliness', 11),
(282, 'Systematically organizing and operating the iED', 11),
(283, 'Ability to perform work at a speed', 11),
(284, 'Ability to perform work with precision', 11),
(285, 'Ability to perform work consistently', 11),
(286, 'IT knowledge', 11),
(287, 'Knowledge of organization / management', 11),
(288, 'Knowledge of trends / hot topics', 11),
(289, 'Σε ποιο βαθμό πιστεύετε ότι ο Marketing & Innovation Manager επιβλέπει σωστά το Τμήμα Marketing;', 11),
(290, 'Πόσο αποτελεσματικά πιστεύετε ότι εκτελεί τα καθήκοντά του ο Marketing & Innovation Manager;', 11),
(291, 'Σε ποιο βαθμό πιστεύετε ότι ο Marketing & Innovation Manager υποστηρίζει τα υπόλοιπα μέλη του τμήματος;', 11),
(292, 'Σε ποιο βαθμό πιστεύετε ότι ο Marketing & Innovation Manager βοηθά στη συνολική επίτευξη των στόχων του τμήματος;', 11),
(293, 'Πόσο αποτελεσματική είναι η επικοινωνία με τον Marketing & Innovation Manager;', 11),
(294, 'Πόσο βαθμολογείτε τη συνεργασία σας με το τμήμα Marketing', 11),
(295, 'Πόσο αποτελεσματικά εργάζεται ο Marketing & Innovation Manager για την ανάπτυξη του τμήματος Business Development;', 11),
(296, 'Quality of work', 12),
(297, 'Speed ​​of work execution', 12),
(298, 'Attempt for individual improvement', 12),
(299, 'Consequence of work', 12),
(300, 'Keeping hours', 12),
(301, 'Attempt to improve his / her work', 12),
(302, 'Knowledge of the function of the department', 12),
(303, 'Knowledge of iED', 12),
(304, 'Knowledge of iED deliverables', 12),
(305, 'Flexibility and adaptability to change', 12),
(306, 'Taking responsibility', 12),
(307, 'Personal effort', 12),
(308, 'Problem Solving Capability', 12),
(309, 'Ability to work under pressure', 12),
(310, 'Contact with colleagues', 12),
(311, 'Interesting, excited and willing to work', 12),
(312, 'Accept advice and instructions', 12),
(313, 'Willingness for education and new knowledge', 12),
(314, 'Volunteering and Sensitivity', 12),
(315, 'Collaboration with colleagues and external partners', 12),
(316, 'Offer help, willingness and service to colleagues', 12),
(317, 'Interpersonal relationships with colleagues', 12),
(318, 'Courtesy / friendliness', 12),
(319, 'Systematically organizing and operating the iED', 12),
(320, 'Ability to perform work at a speed', 12),
(321, 'Ability to perform work with precision', 12),
(322, 'Ability to perform work consistently', 12),
(323, 'IT knowledge', 12),
(324, 'Knowledge of organization / management', 12),
(325, 'Knowledge of trends / hot topics', 12),
(326, 'Attempt for better individual and team results', 12),
(327, 'Πόσο πιστεύετε ότι αυξήθηκαν οι επικοινωνίες του IED με νέους εταίρους', 12),
(328, 'Αποτελεσματική επικοινωνία με τους εταίρους ', 12),
(329, 'Παρακολούθηση του χρονοδιαγράμματος δικτύωσης των calls', 12),
(330, 'Πόσο αποτελεσματικά πιστεύετε ότι εκτελεί τα καθήκοντά του ο Network Assistant Manager?', 12),
(331, 'Quality of work', 13),
(332, 'Speed ​​of work execution', 13),
(333, 'Attempt for individual improvement', 13),
(334, 'Consequence of work', 13),
(335, 'Keeping hours', 13),
(336, 'Attempt to improve his / her work', 13),
(337, 'Knowledge of the function of the department', 13),
(338, 'Knowledge of iED', 13),
(339, 'Knowledge of iED deliverables', 13),
(340, 'Flexibility and adaptability to change', 13),
(341, 'Taking responsibility', 13),
(342, 'Personal effort', 13),
(343, 'Problem Solving Capability', 13),
(344, 'Ability to work under pressure', 13),
(345, 'Contact with colleagues', 13),
(346, 'Interesting, excited and willing to work', 13),
(347, 'Accept advice and instructions', 13),
(348, 'Willingness for education and new knowledge', 13),
(349, 'Volunteering and Sensitivity', 13),
(350, 'Collaboration with colleagues and external partners', 13),
(351, 'Offer help, willingness and service to colleagues', 13),
(352, 'Interpersonal relationships with colleagues', 13),
(353, 'Courtesy / friendliness', 13),
(354, 'Systematically organizing and operating the iED', 13),
(355, 'Ability to perform work at a speed', 13),
(356, 'Ability to perform work with precision', 13),
(357, 'Ability to perform work consistently', 13),
(358, 'IT knowledge', 13),
(359, 'Knowledge of organization / management', 13),
(360, 'Knowledge of trends / hot topics', 13),
(361, 'Attempt for better individual and team results', 13),
(362, 'Τηρούνται τα χρονικά πλαίσια για τη συγγραφή των προτάσεων;', 13),
(363, 'Έχει γραφεί ο προβλεπόμενος αριθμός προτάσεων;', 13),
(364, 'Βαθμολογήστε την ποιότητα των προτάσεων που υποβλήθηκαν από το iED', 13),
(365, 'Quality of work', 14),
(366, 'Speed ​​of work execution', 14),
(367, 'Attempt for individual improvement', 14),
(368, 'Consequence of work', 14),
(369, 'Keeping hours', 14),
(370, 'Attempt to improve his / her work', 14),
(371, 'Knowledge of the function of the department', 14),
(372, 'Knowledge of iED', 14),
(373, 'Knowledge of iED deliverables', 14),
(374, 'Flexibility and adaptability to change', 14),
(375, 'Taking responsibility', 14),
(376, 'Personal effort', 14),
(377, 'Problem Solving Capability', 14),
(378, 'Ability to work under pressure', 14),
(379, 'Contact with colleagues', 14),
(380, 'Interesting, excited and willing to work', 14),
(381, 'Accept advice and instructions', 14),
(382, 'Willingness for education and new knowledge', 14),
(383, 'Volunteering and Sensitivity', 14),
(384, 'Collaboration with colleagues and external partners', 14),
(385, 'Offer help, willingness and service to colleagues', 14),
(386, 'Interpersonal relationships with colleagues', 14),
(387, 'Courtesy / friendliness', 14),
(388, 'Systematically organizing and operating the iED', 14),
(389, 'Ability to perform work at a speed', 14),
(390, 'Ability to perform work with precision', 14),
(391, 'Ability to perform work consistently', 14),
(392, 'IT knowledge', 14),
(393, 'Knowledge of organization / management', 14),
(394, 'Knowledge of trends / hot topics', 14),
(395, 'Σε ποιο βαθμό πιστεύετε ότι ο Network Manager επιβλέπει σωστά τον Network Assistant?', 14),
(396, 'Πόσο πιστεύετε ότι αυξήθηκαν οι επικοινωνίες του IED με νέους εταίρους', 14),
(397, 'Αποτελεσματική επικοινωνία με τους εταίρους ', 14),
(398, 'Παρακολούθηση του χρονοδιαγράμματος δικτύωσης των calls', 14),
(399, 'Πόσο αποτελεσματικά πιστεύετε ότι εκτελεί τα καθήκοντά του ο Network Manager?', 14),
(400, 'Quality of work', 15),
(401, 'Speed ​​of work execution', 15),
(402, 'Attempt for individual improvement', 15),
(403, 'Consequence of work', 15),
(404, 'Keeping hours', 15),
(405, 'Attempt to improve his / her work', 15),
(406, 'Knowledge of the function of the department', 15),
(407, 'Knowledge of iED', 15),
(408, 'Knowledge of iED deliverables', 15),
(409, 'Flexibility and adaptability to change', 15),
(410, 'Taking responsibility', 15),
(411, 'Personal effort', 15),
(412, 'Problem Solving Capability', 15),
(413, 'Ability to work under pressure', 15),
(414, 'Contact with colleagues', 15),
(415, 'Interesting, excited and willing to work', 15),
(416, 'Accept advice and instructions', 15),
(417, 'Willingness for education and new knowledge', 15),
(418, 'Volunteering and Sensitivity', 15),
(419, 'Collaboration with colleagues and external partners', 15),
(420, 'Offer help, willingness and service to colleagues', 15),
(421, 'Interpersonal relationships with colleagues', 15),
(422, 'Courtesy / friendliness', 15),
(423, 'Systematically organizing and operating the iED', 15),
(424, 'Ability to perform work at a speed', 15),
(425, 'Ability to perform work with precision', 15),
(426, 'Ability to perform work consistently', 15),
(427, 'IT knowledge', 15),
(428, 'Knowledge of organization / management', 15),
(429, 'Knowledge of trends / hot topics', 15),
(430, 'Attempt for better individual and team results', 15),
(431, 'Σε ποιο βαθμό πιστεύετε ότι ο SUB Planner παρακολουθεί αποτελεσματικά την υλοποίηση των χρονοδιαγραμμάτων των υποβολών;', 15),
(432, 'Σε ποιο βαθμό πιστεύετε ότι τηρούνται τα χρονοδιαγράμματα που αφορούν την Υποβολή προτάσεων', 15),
(433, 'Προτείνει ο Sub Planner Ιδέες για τη συγγραφή νέων consept notes;', 15),
(434, 'Συντονίζει αποτελεσματικά o Sub Planner τη συγγραφή των προτάσεων;', 15),
(435, 'Πιστεύετε ότι ο Sub Planner συσχετίζει αποτελεσματικά τα κατάλληλα παραδοτέα του iED με το call ', 15),
(436, 'Πιστεύετε ότι ο Sub Planner συσχετίζει αποτελσματικά τα έργα του iED με τα νέα calls', 15),
(437, 'Πόσο σωστά πιστεύετε ότι εκτελεί τα καθήκοντά του ο Sub Planner;', 15),
(438, 'Interpersonal relationships with colleagues', 16),
(439, 'Taking responsibility', 16),
(440, 'Accept advice and instructions', 16),
(441, 'Knowledge of the function of their department', 16),
(442, 'Knowledge of iED', 16),
(443, 'Volunteering and Sensitivity', 16),
(444, 'Interesting, excited and willing to work', 16),
(445, 'Communication with colleagues', 16),
(446, 'Problem solving', 16),
(447, 'Work under pressure', 16),
(448, 'Courtesy / friendliness', 16),
(449, 'Flexibility and adaptability to change', 16),
(450, 'Creativity', 16),
(451, 'Organizational aspects', 16),
(452, 'Quality of work', 16),
(453, 'Cosistency', 16),
(454, 'Speed', 16),
(455, 'How much do you deserve to be the worker of the month?', 16),
(456, 'How much you are willing to work?', 16),
(457, 'How much are you trying to improve?', 16),
(458, 'How much do you keep working hours?', 16),
(459, 'Willingness for education and new knowledge', 16),
(460, 'Offer help, willingness and service to colleagues', 16),
(461, 'Personal effort', 16);

-- --------------------------------------------------------

--
-- Table structure for table `userofdepartment`
--

CREATE TABLE `userofdepartment` (
  `userID` int(11) NOT NULL,
  `DeptID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `userofdepartment`
--

INSERT INTO `userofdepartment` (`userID`, `DeptID`) VALUES
(1, 3),
(2, 3),
(3, 3),
(4, 4),
(5, 4),
(6, 4);

-- --------------------------------------------------------

--
-- Table structure for table `userposition`
--

CREATE TABLE `userposition` (
  `userID` int(11) NOT NULL,
  `positionID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `answers`
--
ALTER TABLE `answers`
  ADD PRIMARY KEY (`userID`,`QuestID`),
  ADD KEY `fk7` (`QuestID`);

--
-- Indexes for table `credentials`
--
ALTER TABLE `credentials`
  ADD PRIMARY KEY (`username`,`password1`),
  ADD KEY `fk_5` (`userID`);

--
-- Indexes for table `ieddepartment`
--
ALTER TABLE `ieddepartment`
  ADD PRIMARY KEY (`DeptID`);

--
-- Indexes for table `ieduser`
--
ALTER TABLE `ieduser`
  ADD PRIMARY KEY (`userID`),
  ADD KEY `rank1` (`rank1`);

--
-- Indexes for table `positions`
--
ALTER TABLE `positions`
  ADD PRIMARY KEY (`positionID`);

--
-- Indexes for table `questionares`
--
ALTER TABLE `questionares`
  ADD PRIMARY KEY (`QID`);

--
-- Indexes for table `questions`
--
ALTER TABLE `questions`
  ADD PRIMARY KEY (`QuestID`),
  ADD KEY `fk6` (`QID`);

--
-- Indexes for table `userofdepartment`
--
ALTER TABLE `userofdepartment`
  ADD PRIMARY KEY (`userID`,`DeptID`),
  ADD KEY `fk_2_dept` (`DeptID`);

--
-- Indexes for table `userposition`
--
ALTER TABLE `userposition`
  ADD PRIMARY KEY (`userID`,`positionID`),
  ADD KEY `fk_4` (`positionID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ieddepartment`
--
ALTER TABLE `ieddepartment`
  MODIFY `DeptID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `ieduser`
--
ALTER TABLE `ieduser`
  MODIFY `userID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `positions`
--
ALTER TABLE `positions`
  MODIFY `positionID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `questionares`
--
ALTER TABLE `questionares`
  MODIFY `QID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `questions`
--
ALTER TABLE `questions`
  MODIFY `QuestID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=462;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `answers`
--
ALTER TABLE `answers`
  ADD CONSTRAINT `fk7` FOREIGN KEY (`QuestID`) REFERENCES `questions` (`QuestID`),
  ADD CONSTRAINT `fk8` FOREIGN KEY (`userID`) REFERENCES `ieduser` (`userID`);

--
-- Constraints for table `credentials`
--
ALTER TABLE `credentials`
  ADD CONSTRAINT `fk_5` FOREIGN KEY (`userID`) REFERENCES `ieduser` (`userID`);

--
-- Constraints for table `questions`
--
ALTER TABLE `questions`
  ADD CONSTRAINT `fk6` FOREIGN KEY (`QID`) REFERENCES `questionares` (`QID`);

--
-- Constraints for table `userofdepartment`
--
ALTER TABLE `userofdepartment`
  ADD CONSTRAINT `fk_1_user` FOREIGN KEY (`userID`) REFERENCES `ieduser` (`userID`),
  ADD CONSTRAINT `fk_2_dept` FOREIGN KEY (`DeptID`) REFERENCES `ieddepartment` (`DeptID`);

--
-- Constraints for table `userposition`
--
ALTER TABLE `userposition`
  ADD CONSTRAINT `fk_3` FOREIGN KEY (`userID`) REFERENCES `ieduser` (`userID`),
  ADD CONSTRAINT `fk_4` FOREIGN KEY (`positionID`) REFERENCES `positions` (`positionID`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
